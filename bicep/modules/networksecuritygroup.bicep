@description('Name of the Network Security Group')
param name string

@description('Location')
param location string = resourceGroup().location

/*
Example:
[
  {
    name: 'AllowExampleInBound'
    properties: {
      priority: 1000
      sourceAddressPrefix: '*'
      protocol: 'Tcp'
      destinationPortRange: '1234'
      access: 'Allow'
      direction: 'Inbound'
      sourcePortRange: '*'
      destinationAddressPrefix: '*'
    }
  }
]
*/
@description('Set the Network Security Group Rules')
param rules array = []

// Deploy the Network Security Group
resource NetworkSecurityGroup 'Microsoft.Network/networkSecurityGroups@2020-07-01' = {
  name: name
  location: location
  properties: {
    securityRules: rules
  }
}

// Return Network Security Group deployment information
output networkSecurityGroupId string = NetworkSecurityGroup.id
