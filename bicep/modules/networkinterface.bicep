@description('Name of the Network Interface')
param name string

@description('Location')
param location string = resourceGroup().location

/* 
Example: 
[
  {
    name: 'ipconfig1'
    properties: {
      subnet: {
        id: '${ExampleVirtualNetwork.id}/subnets/Example-SN'
      }
      privateIPAllocationMethod: 'Dynamic'
      publicIPAddress: {
        id: ExaplePublicIp.id
      }
    }
  }
]
*/
@description('Ip configuration')
param ipConfigurations array

@description('Set the Network Security Group for the Network Interface')
param networkSecurityGroupId string

// Deploy the Network Interface
resource NetworkInterface 'Microsoft.Network/networkInterfaces@2020-06-01' = {
  name: name
  location: location
  properties: {
    ipConfigurations: ipConfigurations
    networkSecurityGroup: {
      id: networkSecurityGroupId
    }
  }
}

// Return the Network Interface deployment information
output networkInterfaceId string = NetworkInterface.id
