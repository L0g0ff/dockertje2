@description('Name of the VM')
param name string

@description('Location')
param location string = resourceGroup().location

@description('Set the Network Interface for the VM')
param networkInterfaceId string

// Note: Don't make the boss angry, keep costs to a minimum! */
@allowed([
  'Standard_LRS'
])
@description('Set the Storage Account type')
param storageAccountType string

// Note: Don't make the boss angry, keep costs to a minimum! */
@allowed([
  'Standard_B2s'
])
@description('Set the size of the VM')
param vmSize string

@description('Set the username')
param adminUsername string

@description('Set the password')
param adminPassword string

// Deploy the Ubuntu VM
resource UbuntuVm 'Microsoft.Compute/virtualMachines@2021-03-01' = {
  name: name
  location: location
  properties: {
    hardwareProfile: {
      vmSize: vmSize
    }
    storageProfile: {
      osDisk: {
        createOption: 'FromImage'
        managedDisk: {
          storageAccountType: storageAccountType
        }
      }
      imageReference: {
        publisher: 'canonical'
        offer: '0001-com-ubuntu-server-focal'
        sku: '20_04-lts'
        version: 'latest'
      }
    }
    networkProfile: {
      networkInterfaces: [
        {
          id: networkInterfaceId
        }
      ]
    }
    osProfile: {
      computerName: name
      adminUsername: adminUsername
      adminPassword: adminPassword
      linuxConfiguration: {
        patchSettings: {
          patchMode: 'ImageDefault'
        }
      }
    }
    diagnosticsProfile: {
      bootDiagnostics: {
        enabled: true
      }
    }
  }
}
