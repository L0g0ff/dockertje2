@description('Name of the Public IP Address')
param name string

@description('Location')
param location string = resourceGroup().location

@allowed([
  'Dynamic'
  'Static'
])
@description('Set the IP allocation method')
param ipAllocationMethod string = 'Dynamic'

// Deploy the Public IP Address
resource PublicIp 'Microsoft.Network/publicIPAddresses@2020-06-01' = {
  name: name
  location: location
  properties: {
    publicIPAllocationMethod: ipAllocationMethod
  }
}

// Return Network PublicIP deployment information
output publicIpId string = PublicIp.id
