@description('Name of the Virtual Network')
param name string

@description('Location')
param location string = resourceGroup().location

/* Example:
[
  '10.0.0.0/16'
]
*/
@description('Virtual Network Address Prefixes')
param addressPrefixes array

/* Example:
[
  {
    name: 'Example-SN'
    properties: {
      addressPrefix: '10.0.0.0/24'
    }
  }      
]
*/
@description('Virtual Network Subnets')
param subnets array

// Deploy the Virtual Network
resource VirtualNetwork 'Microsoft.Network/virtualNetworks@2020-06-01' = {
  name: name
  location: location
  properties: {
    addressSpace: {
      addressPrefixes: addressPrefixes
    }
    subnets: subnets
  }
}

// Return the Virtual Network deployment information
output virtualNetworkId string = VirtualNetwork.id
