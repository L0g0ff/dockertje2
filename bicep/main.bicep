// Default variables
param name string = 'dockertje2'

// Deploy the Virtual Network
module VirtualNetwork 'modules/virtualnetwork.bicep' = {
  name: 'vnet-${name}'
  params: {
    name: 'vnet-${name}'
    addressPrefixes: [
      '10.0.0.0/16'
    ]
    subnets: [
      {
        name: 'snet-${name}'
        properties: {
          addressPrefix: '10.0.0.0/24'
        }
      }
    ]
  }
}

// Deploy the Network Security Group
module NetworkSecurityGroup 'modules/networksecuritygroup.bicep' = {
  name: 'nsg-${name}'
  params: {
    name: 'ngs-${name}'
    rules: [
      {
        name: 'AllowSSHInBound'
        properties: {
          priority: 1000
          sourceAddressPrefix: '*'
          protocol: 'Tcp'
          destinationPortRange: '22'
          access: 'Allow'
          direction: 'Inbound'
          sourcePortRange: '*'
          destinationAddressPrefix: '*'
        }
      }
    ]
  }
}

// Deploy Public IP Address
module PublicIp 'modules/publicip.bicep' = {
  name: 'pip-${name}'
  params: {
    name: 'pip-${name}'
    ipAllocationMethod: 'Static'
  }
}

// Deploy the Bastion Network Interface
module NetworkInterface 'modules/networkinterface.bicep' = {
  name: 'nic-${name}'
  params: {
    name: 'nic-${name}'
    ipConfigurations: [
      {
        name: 'ipconfig1'
        properties: {
          subnet: {
            id: '${VirtualNetwork.outputs.virtualNetworkId}/subnets/snet-${name}'
          }
          privateIPAllocationMethod: 'Dynamic'
          publicIPAddress: {
            id: PublicIp.outputs.publicIpId
          }
        }
      }
    ]
    networkSecurityGroupId: NetworkSecurityGroup.outputs.networkSecurityGroupId
  }
}

// Deploy the Linux VM
module UbuntuVm 'modules/ubuntuvm.bicep' = {
  name: 'vm-${name}'
  params: {
    name: 'vm-${name}'
    networkInterfaceId: NetworkInterface.outputs.networkInterfaceId
    adminUsername: 'dockertje2'
    adminPassword: '..Dockertje2'
    vmSize: 'Standard_B2s'
    storageAccountType: 'Standard_LRS'
  }
}
