# Dockertje2

![Dockertje2](./images/dockertje2.png)

## A Je To!

Welkom. Leuk dat je komt kijken! Even voorstellen hoor! Wij zijn (_fictieve namen_) Mike en Wout. Wij werken voor (_fictieve naam_) Klusjes-ICT. Je raad het al... we helpen je graag! 

Dockertje2 is een project wat laat zien hoe je op een "_out-of-the-box_" manier van denken een Docker project binnen Azure kunt laten landen zonder direct heel veel te moeten betalen per maand.

De opzet van deze repo is in presentatie format, maar wil je spelen met deze oplossing dan staat het je vrij deze beestenboel in je eigen Azure subscription op te spinnen. Veel van deze code kan best een flinke makeover gebruiken. We nodigen je dan ook uit om het te verbeteren. We zien graag jullie PullRequests!

### Bicep

We geven het toe, we zijn een beetje lui geweest. De resource group in Azure hebben we al gemaakt. Om de rest van de omgeving klaar te zetten gebruiken we Azure Bicep. We vertellen hier uiteraard meer over in de presentatie. Het lukte ons niet de storage als module aan te koppelen via Bicep. Volgens ons zijn er nog meer zaken mis gegaan. Vinden jullie ze wellicht?

We balen er zo van. Help jij ons misschien uit de brand door de oplossing aan te bieden via een PullRequest? :-) 

Samengevat. We doen een Ubuntu Linux VM deployment waar we later op kunnen spelen.

```
az deployment group create -f .\bicep\main.bicep -g rg-dockertje2
```

![Azure Services](./images/services.png)

### SSH config

Die config file van onze ssh client is best wel handig. In de presentatie leggen we je uit waarom. 

Wat zouden we aan onze SSH configuratie op de linux server kunnen verbeteren vragen we ons af? Een PullRequest met een bash scriptje wat ons kan helpen vinden we fijn. Misschien iets met `sed` om wat config te veranderen in `/etc/ssh/sshd_config`? :-) 

Configuratie op de client:
```
Host dockertje2
    HostName <ip van onze linux server>
    User dockertje2
    IdentityFile C:\\Users\\UserNameHiero\\.ssh\\dockertje2_rsa
```

### Scripts

We willen natuurlijk niet een flater slaan tijdens onze demo dus we cheaten een beetje door wat scriptjes te hebben voorbereid. Jullie vergeven ons toch wel? 

In de presentatie leggen we uit wat de scripts doen en waarom dit nodig is. 

De scripts zijn alleen wat verbose qua output. We zoeken naar een manier om ze wat stiller te maken zegmaar. Wellicht ook wat validatie hier en daar? Mocht iemand een idee hebben dan zouden we erg blij zijn met een PullRequest :-) 

### Nginx

We hadden echt geen idee hoe we per ducktainer de nginx access en error logs naar een eigen locatie weg konden schrijven. Zou het niet veel netter zijn om nginx ook in een container te zetten? Wie zou dat voor ons kunnen doen? Hmmm...

### Portainer

Por-wat-er??

![Portainer](./images/portainer.png)
