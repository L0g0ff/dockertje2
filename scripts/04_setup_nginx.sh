#!/bin/bash

# Install nginx
apt-get -y install nginx

# Setup the configuration for dockertje2
touch /etc/nginx/sites-enabled/dockertje2.local

echo "
server {
  listen 80;
  server_name dockertje2.local;

  location / {
    proxy_pass http://127.0.0.1:9000;
  }

  location /donald {
    proxy_pass http://127.0.0.1:8081;
  }

  location /kwik {
    proxy_pass http://127.0.0.1:8082;
  }

  location /kwek {
    proxy_pass http://127.0.0.1:8083;
  }

  location /kwak {
    proxy_pass http://127.0.0.1:8084;
  }
}
" > /etc/nginx/sites-enabled/dockertje2.local

# Add dockertje2 to the hosts file
echo "127.0.0.1 dockertje2.local dockertje2" >> /etc/hosts

# Restart Nginx
systemctl restart nginx.service
