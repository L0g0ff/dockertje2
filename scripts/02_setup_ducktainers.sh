#!/bin/bash

# Setup the ducktainers
docker run --name donaldtainer -d -p 8081:8081 hashicorp/http-echo -listen=:8081 -text="Hello from Donald 8081"
docker run --name kwiktainer -d -p 8082:8082 hashicorp/http-echo -listen=:8082 -text="Hello from Kwik 8082"
docker run --name kwektainer -d -p 8083:8083 hashicorp/http-echo -listen=:8083 -text="Hello from Kwek 8083"
docker run --name kwaktainer -d -p 8084:8084 hashicorp/http-echo -listen=:8084 -text="Hello from Kwak 8084"
